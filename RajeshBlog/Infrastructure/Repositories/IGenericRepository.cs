﻿/*
*  Copyright (c) 2017,RajeshBlog. All rights reserved.
*/


using System;
using System.Linq;
using System.Linq.Expressions;

namespace RajeshBlog.Infrastructure.Repositories
{
    /// <summary>
    /// <para>
    /// This interface defines generic operations for accessing entity.
    /// </para>
    /// </summary>
    /// 
    /// <typeparam name="T">
    /// the Identifiable entity type
    /// </typeparam>
    /// 
    /// <remarks>
    /// <para>
    /// Implementations are expected to be effectively thread-safe.
    /// </para>
    /// </remarks>
    /// 
    /// <author>Rajesh Sedhai</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2017, RajeshBlog. All rights reserved.</copyright>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// <para>
        /// Get all entities.
        /// </para>
        /// </summary>
        /// <returns>The result list.</returns>
        IQueryable<T> GetAll() ;

        /// <summary>
        /// <para>
        /// Finds all entities by predicate.
        /// </para>
        /// </summary>
        /// <returns>The matched entities.</returns>
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// <para>
        /// Find an entity by id.
        /// </para>
        /// </summary>
        /// <returns>The matched entity.</returns>
        T Find(long id);

        /// <summary>
        /// Finds as entity by string id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T FindById(string id);

        /// <summary>
        /// <para>
        /// Attaches the entity.
        /// </para>
        /// </summary>
        void Attach(T entity);

        /// <summary>
        /// <para>
        /// Adds the new entity.
        /// </para>
        /// </summary>
        void Add(T entity);

        /// <summary>
        /// <para>
        /// Deletes the entity.
        /// </para>
        /// </summary>
        void Delete(T entity);

        /// <summary>
        /// <para>
        /// Modifies the entity.
        /// </para>
        /// </summary>
        void Edit(T entity);

        /// <summary>
        /// <para>
        /// Saves the changes.
        /// </para>
        /// </summary>
        void Save();
    }
}