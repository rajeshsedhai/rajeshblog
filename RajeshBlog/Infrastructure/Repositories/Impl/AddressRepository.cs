﻿using Entities;
using RajeshBlog.Infrastructure.Repositories.Impl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Impl
{
    public class AddressRepository: GenericRepository<Address>,IAddressRepository
    {
        public AddressRepository(DbContext dbContext)
            :base(dbContext)
        {
        }
    }
}
