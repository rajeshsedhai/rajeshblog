﻿using Entities.Admin;
using RajeshBlog.Infrastructure.Repositories.Impl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Impl
{
    public class AboutUsRepository : GenericRepository<AboutUs>, IAboutUsRepository
    {
        public AboutUsRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
