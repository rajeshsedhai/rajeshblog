﻿/*
*  Copyright (c) 2017,RajeshBlog. All rights reserved.
*/

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using RajeshBlog.Infrastructure.Repositories;

namespace RajeshBlog.Infrastructure.Repositories.Impl
{
    /// <summary>
    /// <para>
    /// This abstract class is a base for all repository classes provided in this module.
    /// </para>
    /// </summary>
    /// 
    /// <typeparam name="T">
    /// The Identifiable entity type.
    /// </typeparam>
    /// 
    /// <remarks>
    /// <para>
    /// This class is immutable (assuming dependencies are not injected more than once) and thread-safe.
    /// </para>
    /// </remarks>
    /// 
    /// <author>Rajesh Sedhai</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2017, RajeshBlog. All rights reserved.</copyright>
    public abstract class GenericRepository<T> : IDisposable, IGenericRepository<T> where T : class
    {
        /// <summary>
        /// <para>
        /// Gets or sets the database context.
        /// </para>
        /// </summary>
        /// <value>The dbContext. The database context instance.</value>
        public DbContext Context
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </para>
        /// </summary>
        protected GenericRepository(DbContext dbContext)
        {
            Context = dbContext;
        }

        /// <summary>
        /// <para>
        /// Get all entities.
        /// </para>
        /// </summary>
        /// <returns>The result list.</returns>
        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = Context.Set<T>();
            return query;
        }

        /// <summary>
        /// <para>
        /// Finds all entities by predicate.
        /// </para>
        /// </summary>
        /// <returns>The matched entities.</returns>
        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = Context.Set<T>().Where(predicate);
            return query;
        }



        /// <summary>
        /// <para>
        /// Find an entity by id.
        /// </para>
        /// </summary>
        /// <returns>The matched entity.</returns>
        public T Find(long id)
        {
            return Context.Set<T>().Find(id);
        }

        /// <summary>
        /// Gets data based on string id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById(string id)
        {
            return Context.Set<T>().Find(id);
        }

        /// <summary>
        /// <para>
        /// Attaches the entity.
        /// </para>
        /// </summary>
        public virtual void Attach(T entity)
        {
            Context.Set<T>().Attach(entity);
        }

        /// <summary>
        /// <para>
        /// Adds the new entity.
        /// </para>
        /// </summary>
        public virtual void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        /// <summary>
        /// <para>
        /// Deletes the entity.
        /// </para>
        /// </summary>
        public virtual void Delete(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// <para>
        /// Modifies the entity.
        /// </para>
        /// </summary>
        public virtual void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// <para>
        /// Saves the changes.
        /// </para>
        /// </summary>
        public virtual void Save()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// <para>
        /// Calls the Dispose method
        /// </para>
        /// </summary>
        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
            }
        }
    }
}
