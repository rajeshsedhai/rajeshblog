// <auto-generated />
namespace Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddedAboutUsEntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedAboutUsEntity));
        
        string IMigrationMetadata.Id
        {
            get { return "201712270205299_Added About Us Entity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
