﻿using Entities.Admin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Entities
{
    public class RajeshBlogDbContext:DbContext
    {
        public RajeshBlogDbContext():base("name=RajeshBlogDbContext")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<AboutUs> AboutUs { get; set; }
    }
}