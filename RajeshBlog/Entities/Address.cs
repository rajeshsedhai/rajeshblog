﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Address
    {
        public int Id { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Suburb { get; set; }
    }
}
