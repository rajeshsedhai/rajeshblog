﻿using Entities.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IAboutUsService
    {
        /// <summary>
        /// Gets all AboutUs.
        /// </summary>
        /// <returns>The AboutUs list.</returns>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        IEnumerable<AboutUs> GetAll();

        /// <summary>
        /// Adds AboutUs
        /// </summary>
        /// <param name="AboutUs">The AboutUs to create.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        long Add(AboutUs AboutUs);

        /// <summary>
        /// Updates AboutUs
        /// </summary>
        /// <param name="AboutUs">The AboutUs to update.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        void Update(AboutUs AboutUs);

        /// <summary>
        /// Deletes AboutUs
        /// </summary>
        /// <param name="AboutUs">The AboutUs to delete.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        void Delete(AboutUs AboutUs);
    }
}
