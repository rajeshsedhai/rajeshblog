﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IAddressService
    {
        /// <summary>
        /// Gets all address.
        /// </summary>
        /// <returns>The address list.</returns>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        IEnumerable<Address> GetAll();

        /// <summary>
        /// Adds address
        /// </summary>
        /// <param name="address">The address to create.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        long Add(Address address);

        /// <summary>
        /// Updates address
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        void Update(Address address);

        /// <summary>
        /// Deletes address
        /// </summary>
        /// <param name="address">The address to delete.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        void Delete(Address address);
    }
}
