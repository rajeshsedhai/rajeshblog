﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IGenericService<T> where T: IdentifiableEntity
    {
        /// <summary>
        /// <para>
        /// Creates a new entity.
        /// </para>
        /// </summary>
        /// <exception cref="ArgumentException">If <paramref name="entity"/> is null or invalid.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        void Create(T entity);

        /// <summary>
        /// <para>
        /// Gets the entity.
        /// </para>
        /// </summary>
        /// <returns>The entity.</returns>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        T Get(long id);

        /// <summary>
        /// <para>
        /// Deletes the entity by given matching id.
        /// </para>
        /// </summary>
        /// <exception cref="EntityNotFoundException">If matching entity is not found.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        void Delete(long id);

        /// <summary>
        /// <para>
        /// Updates the entity.
        /// </para>
        /// </summary>
        /// <exception cref="ArgumentException">If <paramref name="entity"/> is null or invalid.</exception>
        /// <exception cref="EntityNotFoundException">If matching entity is not found.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        void Update(T entity);


        /// <summary>
        /// <para>
        /// Finds all entities by predicate.
        /// </para>
        /// </summary>
        /// <returns>The matched entities.</returns>
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
    }
}
