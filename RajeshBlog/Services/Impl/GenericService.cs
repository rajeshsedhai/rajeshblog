﻿using Entities;
using RajeshBlog.Infrastructure.Repositories;
using Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Services.Impl
{
    /// <summary>
    /// <para>
    /// This abstract class is a base for all services for CRUD operation on the entity.
    /// </para>
    /// </summary>
    /// 
    /// <typeparam name="T">
    /// The Identifiable entity type.
    /// </typeparam>
    /// 
    /// <remarks>
    /// <para>
    /// This class is immutable (assuming dependencies are not injected more than once) and thread-safe.
    /// </para>
    /// </remarks>
    /// 
    /// <author>Rajesh sedhai</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2017. All rights reserved.</copyright>
    public abstract class GenericService<T> : IGenericService<T> where T : IdentifiableEntity
    {
        /// <summary>
        /// Represents the repository.
        /// </summary>
        /// <value>The repository. It can hold any value.</value>
        public IGenericRepository<T> Repository { get; set; }

        /// <summary>
        /// <para>
        /// Creates a new entity.
        /// </para>
        /// </summary>
        /// <exception cref="ArgumentException">If <paramref name="entity"/> is null or invalid.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        public void Create(T entity)
        {
            ValidatorHelper.CheckNotNull(entity, "entity");
            try
            {
                Repository.Add(entity);
                Repository.Save();
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while creating the entity.", ex);
            }
        }

        /// <summary>
        /// <para>
        /// Deletes the entity by given matching id.
        /// </para>
        /// </summary>
        /// <exception cref="EntityNotFoundException">If matching entity is not found.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        public void Delete(long id)
        {
            try
            {
                var entity = Repository.Find(id);
                if (entity == null)
                {
                    throw new EntityNotFoundException(String.Format("Entity with id {0} is not found.", id));
                }
                Repository.Delete(entity);
                Repository.Save();
            }
            catch (Exception ex)
            {
                throw ex is EntityNotFoundException
                    ? ex
                    : new ServiceException("An error occurred while deleting the entity.", ex);
            }
        }

        /// <summary>
        /// <para>
        /// Gets the entity.
        /// </para>
        /// </summary>
        /// <returns>The entity.</returns>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        public T Get(long id)
        {
            try
            {
                return Repository.Find(id);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while getting the entity.", ex);
            }
        }

        /// <summary>
        /// <para>
        /// Updates the entity.
        /// </para>
        /// </summary>
        /// <exception cref="ArgumentException">If <paramref name="entity"/> is null or invalid.</exception>
        /// <exception cref="EntityNotFoundException">If matching entity is not found.</exception>
        /// <exception cref="ServiceException">If any other error occurred while performing the operation.</exception>
        public void Update(T entity)
        {
            ValidatorHelper.CheckNotNull(entity, "entity");
            try
            {
                var existing = Repository.Find(entity.Id);
                if (existing == null)
                {
                    throw new EntityNotFoundException(String.Format("Entity with id {0} is not found.", entity.Id));
                }
                Repository.Edit(existing);
                Repository.Save();
            }
            catch (Exception ex)
            {
                throw ex is EntityNotFoundException
                    ? ex
                    : new ServiceException("An error occurred while updating the entity.", ex);
            }
        }

        /// <summary>
        /// <para>
        /// Finds all entities by predicate.
        /// </para>
        /// </summary>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        /// <returns>The matched entities.</returns>
        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return Repository.FindBy(predicate);
            }
            catch (Exception ex)
            {
                throw ex is EntityNotFoundException
                    ? ex
                    : new ServiceException("An error occurred while updating the entity.", ex);
            }
        }
    }
}
