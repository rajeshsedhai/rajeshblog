﻿using Entities;
using Entities.Admin;
using Infrastructure.Repositories;
using Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Impl
{

    public class AboutUsService : BaseService<AboutUs>, IAboutUsService
    {
        public AboutUsService(IAboutUsRepository repo)
            : base(repo)
        {
        }
        /// <summary>
        /// Gets all AboutUs.
        /// </summary>
        /// <returns>The AboutUs list.</returns>
        /// <exception cref="AboutUs">If any error occurred while performing the operation.</exception>
        public IEnumerable<AboutUs> GetAll()
        {
            try
            {
                return Repository.GetAll().ToList();
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while getting the AboutUs", ex);
            }
        }

        /// <summary>
        /// Add category
        /// </summary>
        /// <param name="category">The AboutUs to create.</param>
        /// <returns>AboutUs</returns>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public long Add(AboutUs AboutUs)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Title == AboutUs.Title).FirstOrDefault();
                if (lst != null)
                {
                    throw new ServiceException("AboutUs name already exists.");
                }
                else
                {
                    Repository.Add(AboutUs);
                    Repository.Save();
                }
                return AboutUs.Id;
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while adding AboutUs", ex);
            }
        }

        /// <summary>
        /// Update AboutUs
        /// </summary>
        /// <param name="AboutUs">The AboutUs to update.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public void Update(AboutUs AboutUs)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Id == AboutUs.Id).FirstOrDefault();
                if (lst == null)
                {
                    throw new EntityNotFoundException("AboutUs not found");
                }
                else
                {
                    Repository.Edit(lst);
                    Repository.Save();
                };
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while updating AboutUs", ex);
            }
        }

        /// <summary>
        /// Delete AboutUs
        /// </summary>
        /// <param name="AboutUs">The AboutUs to delete.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public void Delete(AboutUs AboutUs)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Id == AboutUs.Id).FirstOrDefault();
                if (lst == null)
                {
                    throw new EntityNotFoundException("AboutUs not found");
                }
                else
                {
                    Repository.Delete(lst);
                    Repository.Save();
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while deleting AboutUs", ex);
            }
        }


    }
}
