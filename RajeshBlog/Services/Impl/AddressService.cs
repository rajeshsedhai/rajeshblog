﻿using Entities;
using Infrastructure.Repositories;
using Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Impl
{
    public class AddressService : BaseService<Address>, IAddressService
    {
        public AddressService(IAddressRepository repo)
            : base(repo)
        {
        }
        /// <summary>
        /// Gets all address.
        /// </summary>
        /// <returns>The address list.</returns>
        /// <exception cref="Address">If any error occurred while performing the operation.</exception>
        public IEnumerable<Address> GetAll()
        {
            try
            {
                return Repository.GetAll().ToList();
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while getting the address", ex);
            }
        }

        /// <summary>
        /// Add category
        /// </summary>
        /// <param name="category">The address to create.</param>
        /// <returns>address</returns>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public long Add(Address address)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Region == address.Region).FirstOrDefault();
                if (lst != null)
                {
                    throw new ServiceException("Address name already exists.");
                }
                else
                {
                    Repository.Add(address);
                    Repository.Save();
                }
                return address.Id;
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while adding address", ex);
            }
        }

        /// <summary>
        /// Update address
        /// </summary>
        /// <param name="address">The address to update.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public void Update(Address address)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Id == address.Id).FirstOrDefault();
                if (lst == null)
                {
                    throw new EntityNotFoundException("address not found");
                }
                else
                {
                    Repository.Edit(lst);
                    Repository.Save();
                };
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while updating address", ex);
            }
        }

        /// <summary>
        /// Delete address
        /// </summary>
        /// <param name="address">The address to delete.</param>
        /// <exception cref="ServiceException">If any error occurred while performing the operation.</exception>
        public void Delete(Address address)
        {
            try
            {
                var lst = Repository.FindBy(m => m.Id == address.Id).FirstOrDefault();
                if (lst == null)
                {
                    throw new EntityNotFoundException("Address not found");
                }
                else
                {
                    Repository.Delete(lst);
                    Repository.Save();
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException("An error occurred while deleting address", ex);
            }
        }


    }
}
