﻿using RajeshBlog.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Impl
{
    /// <summary>
    /// <para>
    /// This abstract class is a base for all services which exposes the generic repository.
    /// </para>
    /// </summary>
    public class BaseService<T> where T:class
    {
        /// <summary>
        /// Represents the repository.
        /// </summary>
        /// <value>The repository. It can hold any value.</value>
        public IGenericRepository<T> Repository { get; set; }

        /// <summary>
        /// Initializes the new instance of <see cref="BaseService{T}"/>
        /// </summary>
        /// <param name="repo">The repository for entity.</param>
        protected BaseService(IGenericRepository<T> repo)
        {
            Repository = repo;
        }
    }
}
