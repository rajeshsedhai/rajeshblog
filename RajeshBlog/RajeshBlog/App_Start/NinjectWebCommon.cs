﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Ninject;
using Ninject.Web.Common;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using RajeshBlog.App_Start;
using System.Data.Entity;
using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject.Web.WebApi;
using Infrastructure.Repositories;
using Infrastructure.Repositories.Impl;
using Services.Impl;
using Services;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]
namespace RajeshBlog.App_Start
{
    /// <summary>
    /// This class is used to inject the dependency through out the application
    /// </summary>
    public static class NinjectWebCommon
    {
        /// <summary>
        /// Represents the <see cref="Bootstrapper"/>.
        /// </summary>
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<RajeshBlogDbContext>().InRequestScope();
            kernel.Bind<IRoleStore<IdentityRole, string>>()
               .ToConstructor(u => new RoleStore<IdentityRole>(kernel.Get<DbContext>()))
               .InRequestScope();
            #region Repository
            kernel.Bind<IAddressRepository>().To<AddressRepository>();
            kernel.Bind<IAboutUsRepository>().To<AboutUsRepository>();
            #endregion
            #region Service
            kernel.Bind<IAddressService>().To<AddressService>();
            kernel.Bind<IAboutUsService>().To<AboutUsService>();
            #endregion
        }
    }
}
