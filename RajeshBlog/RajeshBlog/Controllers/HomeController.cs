﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RajeshBlog.Controllers
{
    public class HomeController : Controller
    {
        private IAboutUsService _aboutUsService;

        public HomeController(IAboutUsService aboutUsService)
        {
            _aboutUsService = aboutUsService;
        }

        public ActionResult Index()
        {
            var data = _aboutUsService.GetAll();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}