﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RajeshBlog.Startup))]
namespace RajeshBlog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
