﻿/*
*  Copyright (c) 2017, Rajesh Sedhai. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Utilities
{
    /// <summary>
    /// <para>
    /// This class is the helper class for common function
    /// </para>
    /// </summary>
    /// 
    /// <author>Rajesh Sedhai</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2017, Rajesh sedhai. All rights reserved.</copyright>
    /// 
    public class CommonHelper
    {
        /// <summary>
        /// Convert data table to type list.
        /// </summary>
        /// <param name="dt">The data table</param>
        /// <returns>The list.</returns>
        public static List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name))
                    {
                        PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                        pro.SetValue(objT, row[pro.Name] == DBNull.Value ? null : Convert.ChangeType(row[pro.Name], pI.PropertyType));
                    }
                }
                return objT;
            }).ToList();
        }
    }
}
