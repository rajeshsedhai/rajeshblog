﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class ValidatorHelper
    {
        /// <summary>
        /// <para>
        /// Checks whether the given object is null.
        /// </para>
        /// </summary>
        ///
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the argument being checked.
        /// </param>
        ///
        /// <exception cref="ArgumentNullException">
        /// If object is null.
        /// </exception>
        public static void CheckNotNull(object value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentException(string.Format("'{0}' is Required.", parameterName));
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given string is null or empty.
        /// </para>
        /// </summary>
        ///
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the argument being checked.
        /// </param>
        ///
        /// <exception cref="ArgumentNullException">
        /// If the string is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If the given string is empty string.
        /// </exception>
        public static void CheckNotNullOrEmpty(string value, string parameterName)
        {
            CheckNotNull(value, parameterName);
            if (value.Trim().Length == 0)
            {
                throw new ArgumentException(string.Format("'{0}' is Required.", parameterName));
            }
        }
    }
}
